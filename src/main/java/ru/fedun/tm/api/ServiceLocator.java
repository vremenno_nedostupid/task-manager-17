package ru.fedun.tm.api;

import ru.fedun.tm.api.service.*;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ICommandService getCommandService();

    ICrudService<Task> getTaskService();

    ICrudService<Project> getProjectService();

    IDomainService getDomainService();

}
