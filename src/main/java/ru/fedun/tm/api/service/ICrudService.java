package ru.fedun.tm.api.service;

import java.util.List;

public interface ICrudService<T> {

    void create(String userId, String title);

    void create(String userId, String title, String description);

    List<T> findAll(String userId);

    List<T> findAll();

    void clear(String userId);

    T getOneById(String userId, String id);

    T getOneByIndex(String userId, Integer index);

    T getOneByTitle(String userId, String title);

    T updateById(String userId, String id, String title, String description);

    T updateByIndex(String userId, Integer index, String title, String description);

    T removeOneById(String userId, String id);

    T removeOneByIndex(String userId, Integer index);

    T removeOneByTitle(String userId, String title);

    void load(List<T> o);

    void load(T... o);

}
