package ru.fedun.tm.dto;

import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class Domain implements Serializable {

    private List<Project> projects = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<User> users = new ArrayList<>();


    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
